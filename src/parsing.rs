use crate::inner::*;

mod base_instruction;
mod bytes;
mod compressed;
mod error;

pub use base_instruction::*;
pub use bytes::*;
pub use compressed::*;
pub use error::*;

macro_rules! imm {
    ($bytes:expr, $imm_ranges:expr) => {
        parse_immediate($bytes, &$imm_ranges, &[]).unwrap()
    };
    ($bytes:expr, $imm_ranges:expr, $order_ranges:expr) => {
        parse_immediate($bytes, &$imm_ranges, &$order_ranges).unwrap()
    };
}
pub(crate) use imm;

pub struct InstructionParser<'a> {
    bytes: &'a [u8],
    addr: Addr,
}

impl<'a> Iterator for InstructionParser<'a> {
    type Item = PResult<Instruction>;
    fn next(&mut self) -> Option<Self::Item> {
        if self.bytes.is_empty() {
            None
        } else {
            let res = match parse_instruction(self.bytes, self.addr) {
                Ok(instruction) => {
                    let instr_size = if instruction.compressed { 2 } else { 4 };
                    self.addr.0 += instr_size;
                    self.bytes = &self.bytes[instr_size as usize..];
                    Ok(instruction)
                }
                Err(err) => {
                    self.bytes = &[];
                    Err(err)
                }
            };
            Some(res)
        }
    }
}

pub struct NoErrorInstructionParser<'a> {
    bytes: &'a [u8],
    addr: Addr,
}

impl<'a> Iterator for NoErrorInstructionParser<'a> {
    type Item = Instruction;
    fn next(&mut self) -> Option<Self::Item> {
        if self.bytes.is_empty() {
            None
        } else {
            match parse_instruction_no_error(self.bytes, self.addr) {
                Some(instruction) => {
                    let instr_size = if instruction.compressed { 2 } else { 4 };
                    self.addr.0 += instr_size;
                    self.bytes = &self.bytes[instr_size as usize..];
                    Some(instruction)
                }
                None => {
                    self.bytes = &[];
                    None
                }
            }
        }
    }
}

pub fn parse_bytes(bytes: &[u8], addr: Addr) -> impl Iterator<Item = PResult<Instruction>> + '_ {
    InstructionParser { addr, bytes }
}

pub fn parse_bytes_no_error(
    bytes: &[u8],
    addr: Addr,
) -> impl Iterator<Item = Instruction> + '_ {
    NoErrorInstructionParser { addr, bytes }
}
