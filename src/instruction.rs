mod csr;
mod id;
mod instr;
mod reg;

pub use csr::*;
pub use id::*;
pub use instr::*;
pub use reg::*;

// pub struct Instruction {}
