use crate::inner::*;
use Reg::*;

use super::imm;
use BaseInstruction::*;

pub fn parse_compressed_instruction(addr: Addr, bytes: u32) -> PResult<Instruction> {
    let opcode = bytes & 3;
    match opcode {
        0b01 => parse_compressed_instruction01(addr, bytes),
        0b00 => parse_compressed_instruction00(addr, bytes),
        0b10 => parse_compressed_instruction10(addr, bytes),
        0b11 => Err(UnknownOpCode(addr, 0b11)),
        _ => unreachable!(),
    }
}

pub fn parse_compressed_instruction01(addr: Addr, bytes: u32) -> PResult<Instruction> {
    let func3 = imm!(bytes, [15, -13]);
    match func3 {
        0b000 => {
            let imm5: u8 = imm!(bytes, [11, -7]);
            if imm5 == 0 {
                Ok(Instruction {
                    instr: F(FInstruction::NOP),
                    compressed: true,
                    bytes,
                    addr,
                })
            } else {
                let rsd = Reg::try_from(imm5, addr)?;
                let imm6: u16 = imm!(bytes, [12, 6, -2]);
                Ok(Instruction {
                    instr: I(IInstruction {
                        id: IId::BinOp(BinOpIId::ADDI),
                        rd: rsd,
                        rs: rsd,
                        imm12: imm6,
                    }),
                    compressed: true,
                    bytes,
                    addr,
                })
            }
        }
        0b001 | 0b101 => {
            let imm11 = imm!(bytes, [12, -2], [11, 4, 9, 8, 10, 6, 7, 3, -1, 5]);
            match func3 {
                0b001 => Ok(Instruction {
                    instr: J(JInstruction {
                        id: JId::JAL,
                        rd: Reg::ra,
                        imm20: imm11,
                    }),
                    compressed: true,
                    bytes,
                    addr,
                }),
                0b101 => Ok(Instruction {
                    instr: J(JInstruction {
                        id: JId::JAL,
                        rd: Reg::zero,
                        imm20: imm11,
                    }),
                    compressed: true,
                    bytes,
                    addr,
                }),
                _ => unreachable!(),
            }
        }
        0b010 => {
            let rd = Reg::try_from(imm!(bytes, [11, -7]), addr)?;
            let imm6 = imm!(bytes, [12, 6, -2]);
            let instr = Instruction {
                instr: I(IInstruction {
                    id: IId::BinOp(BinOpIId::ADDI),
                    rd,
                    rs: zero,
                    imm12: imm6,
                }),
                compressed: true,
                bytes,
                addr,
            };
            if matches!(rd, zero) {
                Err(InvalidInstruction(instr))
            } else {
                Ok(instr)
            }
        }
        0b011 => {
            let rd = Reg::try_from(imm!(bytes, [11, -7]), addr)?;
            if matches!(rd, sp) {
                let imm6 = imm!(bytes, [12, 6, -2], [9, 4, 6, 8, 7, 5]);
                Ok(Instruction {
                    instr: I(IInstruction {
                        id: IId::BinOp(BinOpIId::ADDI),
                        rd: sp,
                        rs: sp,
                        imm12: imm6,
                    }),
                    compressed: true,
                    bytes,
                    addr,
                })
            } else {
                let imm6 = imm!(bytes, [12, 6, -2], [17, 16, -12]);
                let instr = Instruction {
                    instr: U(UInstruction {
                        id: UId::LUI,
                        rd,
                        imm20: imm6,
                    }),
                    compressed: true,
                    bytes,
                    addr,
                };
                if matches!(rd, zero) {
                    Err(InvalidInstruction(instr))
                } else {
                    Ok(instr)
                }
            }
        }
        0b100 => {
            let func2 = imm!(bytes, [11, 10]);
            let rd = Reg::try_from_compressed(imm!(bytes, [9, -7]), addr)?;
            match func2 {
                0b00 | 0b01 | 0b10 => {
                    let imm6 = imm!(bytes, [12, 6, -2]);
                    let id = match func2 {
                        0b00 => IId::BinOp(BinOpIId::SRLI),
                        0b01 => IId::BinOp(BinOpIId::SRAI),
                        0b10 => IId::BinOp(BinOpIId::ANDI),
                        _ => unreachable!(),
                    };

                    Ok(Instruction {
                        instr: I(IInstruction {
                            id,
                            rd,
                            rs: rd,
                            imm12: imm6,
                        }),
                        compressed: true,
                        bytes,
                        addr,
                    })
                }
                11 => {
                    let func2 = imm!(bytes, [6, 5]);
                    let id = match func2 {
                        0b00 => RId::SUB,
                        0b01 => RId::XOR,
                        0b10 => RId::OR,
                        0b11 => RId::AND,
                        _ => unreachable!(),
                    };

                    let rs2 = Reg::try_from_compressed(imm!(bytes, [4, -2]), addr)?;
                    Ok(Instruction {
                        instr: R(RInstruction {
                            id,
                            rd,
                            rs1: rd,
                            rs2,
                        }),
                        compressed: true,
                        bytes,
                        addr,
                    })
                }
                _ => unreachable!(),
            }
        }
        0b110 | 0b111 => {
            let rs1 = Reg::try_from_compressed(imm!(bytes, [9, -7]), addr)?;
            let imm8 = imm!(bytes, [12, -10, 6, -2], [8, 4, 3, 7, 6, 2, 1, 5]);
            let id = match func3 {
                0b110 => BId::BEQ,
                0b111 => BId::BNE,
                _ => unreachable!(),
            };

            Ok(Instruction {
                instr: B(BInstruction {
                    id,
                    rs1,
                    rs2: zero,
                    imm12: imm8,
                }),
                compressed: true,
                bytes,
                addr,
            })
        }
        _ => unreachable!(),
    }
}
// 0b101 => Ok(Instruction { instr: BaseInstruction::I(IInstruction { id: IId::JALR, rd: (), rs: (), imm12: () }), compressed: (), bytes: (), addr: () })

pub fn parse_compressed_instruction00(addr: Addr, bytes: u32) -> PResult<Instruction> {
    let func3 = imm!(bytes, [15, -13]);
    match func3 {
        0b000 => {
            let rd = Reg::try_from_compressed(imm!(bytes, [4, -2]), addr)?;
            let imm8 = imm!(bytes, [12, -5], [5, 4, 9, -6, 2, 3]);
            let instr = Instruction {
                instr: I(IInstruction {
                    id: IId::BinOp(BinOpIId::ADDI),
                    rd,
                    rs: sp,
                    imm12: imm8,
                }),
                compressed: true,
                bytes,
                addr,
            };
            if imm8 != 0 {
                Ok(instr)
            } else {
                Err(IllegalInstruction(instr))
            }
        }
        0b010 | 0b110 => {
            let reg = Reg::try_from_compressed(imm!(bytes, [4, -2]), addr)?;
            let rs1 = Reg::try_from_compressed(imm!(bytes, [9, -7]), addr)?;
            let imm5 = imm!(bytes, [12, -10, 6, 5], [5, -3, 2, 6]);
            let base_instr = match func3 {
                0b010 => I(IInstruction {
                    id: IId::Load(LoadIId::LW),
                    rd: reg,
                    rs: rs1,
                    imm12: imm5,
                }),
                0b110 => S(SInstruction {
                    id: SId::SW,
                    rs1,
                    rs2: reg,
                    imm12: imm5,
                }),
                _ => unreachable!(),
            };
            Ok(Instruction {
                instr: base_instr,
                compressed: true,
                bytes,
                addr,
            })
        }
        _ => Err(UnknownFuncCode(addr, OpCode::Compressed(0b00), func3)),
    }
}

pub fn parse_compressed_instruction10(addr: Addr, bytes: u32) -> PResult<Instruction> {
    let func3 = imm!(bytes, [15, -13]);
    match func3 {
        0b000 => {
            let rd = Reg::try_from(imm!(bytes, [11, -7]), addr)?;
            let imm6 = imm!(bytes, [12, 6, -2]);
            let instr = Instruction {
                instr: I(IInstruction {
                    id: IId::BinOp(BinOpIId::SLLI),
                    rd,
                    rs: rd,
                    imm12: imm6,
                }),
                compressed: true,
                bytes,
                addr,
            };
            if matches!(rd, zero) {
                Err(InvalidInstruction(instr))
            } else {
                Ok(instr)
            }
        }
        0b010 => {
            let rd = Reg::try_from(imm!(bytes, [11, -7]), addr)?;
            let imm6 = imm!(bytes, [12, 6, -2], [5, -2, 7, 6]);
            let instr = Instruction {
                instr: I(IInstruction {
                    id: IId::Load(LoadIId::LW),
                    rd,
                    rs: sp,
                    imm12: imm6,
                }),
                compressed: true,
                bytes,
                addr,
            };

            if matches!(rd, zero) {
                Err(InvalidInstruction(instr))
            } else {
                Ok(instr)
            }
        }

        0b100 => {
            let func1: u8 = imm!(bytes, [12]);
            let reg2 = Reg::try_from(imm!(bytes, [6, -2]), addr)?;
            let reg1 = Reg::try_from(imm!(bytes, [11, -7]), addr)?;
            let base_instr = match (func1, reg1, reg2) {
                (0, rs1, zero) => I(IInstruction {
                    id: IId::JALR,
                    rd: zero,
                    rs: rs1,
                    imm12: 0,
                }),
                (0, rd, rs2) => R(RInstruction {
                    id: RId::ADD,
                    rd,
                    rs1: zero,
                    rs2,
                }),
                (1, zero, zero) => EI(EIInstruction::SI(SpecialInstruction { id: EIId::EBREAK })),
                (1, rs1, zero) => I(IInstruction {
                    id: IId::JALR,
                    rd: ra,
                    rs: rs1,
                    imm12: 0,
                }),
                (1, rd, rs2) => R(RInstruction {
                    id: RId::ADD,
                    rd,
                    rs1: rd,
                    rs2,
                }),
                _ => unreachable!(),
            };

            let instr = Instruction {
                instr: base_instr,
                compressed: true,
                bytes,
                addr,
            };

            if matches!(reg1, zero) {
                Err(PError::InvalidInstruction(instr))
            } else {
                Ok(instr)
            }
        }
        0b110 => {
            let rs2 = Reg::try_from(imm!(bytes, [6, -2]), addr)?;
            let imm6 = imm!(bytes, [12, -7], [5, -2, 7, 6]);
            Ok(Instruction {
                instr: S(SInstruction {
                    id: SId::SW,
                    rs1: sp,
                    rs2,
                    imm12: imm6,
                }),
                compressed: true,
                bytes,
                addr,
            })
        }
        _ => Err(UnknownFuncCode(addr, OpCode::Compressed(0b10), func3)),
    }
}
