use std::fmt::{self, Debug, Display, Formatter};

use crate::inner::*;

#[derive(Clone, Copy)]
pub enum PError {
    UnknownOpCode(Addr, u8),
    UnknownFuncCode(Addr, OpCode, u32),
    UnknownReg(Addr, u8),
    InvalidImmediate(Instruction),
    InvalidInstruction(Instruction),
    IllegalInstruction(Instruction),
    EndOfBytes,
    UnexpectedEndOfBytes,
}

impl Display for PError {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        match self {
            UnknownOpCode(addr, opcode) => {
                write!(f, "unknown opcode at address {:08x}: {opcode:07b}", addr.0)
            }
            UnknownFuncCode(addr, opcode, func) => {
                write!(
                    f,
                    "unknown func at address {:08x} with opcode {:?}: 0x{func:x}",
                    addr.0, opcode
                )
            }
            UnknownReg(addr, reg) => {
                write!(f, "unknown reg at address {:08x}: x{reg}", addr.0)
            }
            InvalidImmediate(instr) => {
                write!(f, "invalid immediate at address {}", instr)
            }
            InvalidInstruction(instr) => {
                write!(f, "invalid instruction at address {}", instr)
            }
            IllegalInstruction(instr) => {
                write!(f, "illegal instruction at address {}", instr)
            }
            EndOfBytes => write!(f, "end of bytes"),
            UnexpectedEndOfBytes => write!(f, "unexpected end of bytes"),
        }
    }
}

impl Debug for PError {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        Display::fmt(self, f)
    }
}

pub use PError::*;

pub type PResult<T> = Result<T, PError>;
