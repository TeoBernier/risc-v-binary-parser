use super::imm;
use crate::inner::*;

use BaseInstruction::*;

pub fn parse_instruction(bytes: &[u8], addr: Addr) -> PResult<Instruction> {
    let &fst_bytes = bytes.get(0).ok_or(EndOfBytes)?;
    let &snd_bytes = bytes.get(1).ok_or(UnexpectedEndOfBytes)?;
    let lsb_op = fst_bytes & 3;

    if lsb_op != 3 {
        let compressed_instruction_bytes = ((snd_bytes as u32) << 8) + (fst_bytes as u32);
        if let Some(&trd_bytes) = bytes.get(2) {
            if let Some(&fth_bytes) = bytes.get(3) {
                let instruction_bytes = ((fth_bytes as u32) << 24)
                    + ((trd_bytes as u32) << 16)
                    + ((snd_bytes as u32) << 8)
                    + (fst_bytes as u32);
                let res = parse_full_instruction(addr, instruction_bytes);
                if res.is_ok() {
                    return res;
                }
            }
        }
        return parse_compressed_instruction(addr, compressed_instruction_bytes);
    }

    let &trd_bytes = bytes.get(2).ok_or(UnexpectedEndOfBytes)?;
    let &fth_bytes = bytes.get(3).ok_or(UnexpectedEndOfBytes)?;

    let instruction_bytes = ((fth_bytes as u32) << 24)
        + ((trd_bytes as u32) << 16)
        + ((snd_bytes as u32) << 8)
        + (fst_bytes as u32);

    parse_full_instruction(addr, instruction_bytes)
}

pub fn transform_err(
    addr: Addr,
    bytes: u32,
    compressed: bool,
    res: PResult<Instruction>,
) -> Option<Instruction> {
    let base_instr = match res {
        Ok(instr) => return Some(instr),
        Err(EndOfBytes) | Err(UnexpectedEndOfBytes) => return None,
        Err(IllegalInstruction(_)) => F(FInstruction::Illegal),
        _ => F(FInstruction::Unimplemented),
    };

    Some(Instruction {
        instr: base_instr,
        compressed,
        bytes,
        addr,
    })
}

pub fn parse_instruction_no_error(bytes: &[u8], addr: Addr) -> Option<Instruction> {
    let &fst_bytes = bytes.get(0)?;
    let &snd_bytes = bytes.get(1)?;
    let lsb_op = fst_bytes & 3;

    if lsb_op != 3 {
        let compressed_instruction_bytes = ((snd_bytes as u32) << 8) + (fst_bytes as u32);
        if let Some(&trd_bytes) = bytes.get(2) {
            if let Some(&fth_bytes) = bytes.get(3) {
                let instruction_bytes = ((fth_bytes as u32) << 24)
                    + ((trd_bytes as u32) << 16)
                    + ((snd_bytes as u32) << 8)
                    + (fst_bytes as u32);
                let res = parse_full_instruction(addr, instruction_bytes);
                if res.is_ok() {
                    return res.ok();
                }
            }
        }
        return transform_err(
            addr,
            compressed_instruction_bytes,
            true,
            parse_compressed_instruction(addr, compressed_instruction_bytes),
        );
    }

    let &trd_bytes = bytes.get(2)?;
    let &fth_bytes = bytes.get(3)?;

    let instruction_bytes = ((fth_bytes as u32) << 24)
        + ((trd_bytes as u32) << 16)
        + ((snd_bytes as u32) << 8)
        + (fst_bytes as u32);
    transform_err(
        addr,
        instruction_bytes,
        false,
        parse_full_instruction(addr, instruction_bytes),
    )
}

pub fn parse_full_instruction(addr: Addr, bytes: u32) -> PResult<Instruction> {
    use OpCode::*;

    let opcode = OpCode::try_from(imm!(bytes, [6, NEG_0]), addr)?;

    match opcode {
        BinOp => parse_rinstruction(addr, opcode, bytes),
        ABinOp => parse_atomic_rinstruction(addr, opcode, bytes),
        BinOpImm | Load | JALR => parse_iinstruction(addr, opcode, bytes),
        Environment => parse_eiinstruction(addr, opcode, bytes),
        Fence | Nop => parse_finstruction(addr, opcode, bytes),
        Store => parse_sinstruction(addr, opcode, bytes),
        Branch => parse_binstruction(addr, opcode, bytes),
        AUIPC | LUI => parse_uinstruction(addr, opcode, bytes),
        JAL => parse_jinstruction(addr, opcode, bytes),
        _ => unreachable!(),
    }
}

pub fn parse_rinstruction(addr: Addr, _opcode: OpCode, bytes: u32) -> PResult<Instruction> {
    let rd = Reg::try_from(imm!(bytes, [11, -7]), addr)?;
    let id = RId::try_from(imm!(bytes, [31, -25, 14, -12]), addr)?;
    let rs1 = Reg::try_from(imm!(bytes, [19, -15]), addr)?;
    let rs2 = Reg::try_from(imm!(bytes, [24, -20]), addr)?;

    Ok(Instruction {
        instr: R(RInstruction { id, rd, rs1, rs2 }),
        compressed: false,
        bytes,
        addr,
    })
}

pub fn parse_atomic_rinstruction(addr: Addr, _opcode: OpCode, bytes: u32) -> PResult<Instruction> {
    let rd = Reg::try_from(imm!(bytes, [11, -7]), addr)?;
    let id = ARId::try_from(imm!(bytes, [31, -27, 14, -12]), addr)?;
    let rs1 = Reg::try_from(imm!(bytes, [19, -15]), addr)?;
    let rs2 = Reg::try_from(imm!(bytes, [24, -20]), addr)?;
    let rl = ((bytes >> 25) & 0x1) != 0;
    let aq = ((bytes >> 26) & 0x1) != 0;

    Ok(Instruction {
        instr: AR(ARInstruction {
            id,
            rd,
            rs1,
            rs2,
            rl,
            aq,
        }),
        compressed: false,
        bytes,
        addr,
    })
}

pub fn parse_iinstruction(addr: Addr, opcode: OpCode, bytes: u32) -> PResult<Instruction> {
    let rd = Reg::try_from(imm!(bytes, [11, -7]), addr)?;
    let rs = Reg::try_from(imm!(bytes, [19, -15]), addr)?;
    let func3 = imm!(bytes, [14, -12]);
    let imm12 = imm!(bytes, [31, -20]);

    match opcode {
        OpCode::BinOpImm => {
            let (id, imm12) = match (BinOpIId::try_from(func3 as u16, addr)?, imm12 >> 5_u8) {
                (BinOpIId::SRLI, 0x20) => (BinOpIId::SRAI, imm12 & 0x1F),
                (id, _) => (id, imm12),
            };

            let instr = Instruction {
                instr: I(IInstruction {
                    id: IId::BinOp(id),
                    rd,
                    rs,
                    imm12,
                }),
                compressed: false,
                bytes,
                addr,
            };

            if matches!(id, BinOpIId::SRLI | BinOpIId::SLLI) && imm12 > 0x1F {
                Err(InvalidImmediate(instr))
            } else {
                Ok(instr)
            }
        }
        OpCode::Load => Ok(Instruction {
            instr: I(IInstruction {
                id: IId::Load(LoadIId::try_from(func3, addr)?),
                rd,
                rs,
                imm12,
            }),
            compressed: false,
            bytes,
            addr,
        }),
        OpCode::JALR => {
            if func3 != 0 {
                Err(UnknownFuncCode(addr, OpCode::JALR, func3 as u32))
            } else {
                Ok(Instruction {
                    instr: I(IInstruction {
                        id: IId::JALR,
                        rd,
                        rs,
                        imm12,
                    }),
                    compressed: false,
                    bytes,
                    addr,
                })
            }
        }
        _ => unreachable!(),
    }
}

pub fn parse_eiinstruction(addr: Addr, _opcode: OpCode, bytes: u32) -> PResult<Instruction> {
    let rd = Reg::try_from(imm!(bytes, [11, -7]), addr)?;
    let func3 = imm!(bytes, [14, -12]);
    let id = EIId::try_from(func3, addr)?;
    let imm5 = imm!(bytes, [19, -15]);
    let imm12 = imm!(bytes, [31, -20]);
    use EIId::*;
    use EIInstruction::*;
    match id {
        ECALL => {
            let func15 = (imm12 << 3) + func3 as u16;

            let id = EIId::try_from(func15 & 0x7F00, addr).or(EIId::try_from(func15, addr))?;

            match id {
                SFENCEVMA => {
                    let rs1 = Reg::try_from(imm5, addr)?;
                    let rs2 = Reg::try_from((imm12 & 0x1F) as u8, addr)?;

                    Ok(Instruction {
                        instr: EI(SFenceInstruction(rs1, rs2)),
                        compressed: false,
                        bytes,
                        addr,
                    })
                }
                _ => {
                    let instr = Instruction {
                        instr: EI(SI(SpecialInstruction { id })),
                        compressed: false,
                        bytes,
                        addr,
                    };

                    if imm5 != 0 {
                        Err(InvalidInstruction(instr))
                    } else {
                        Ok(instr)
                    }
                }
            }
        }

        CSRRW | CSRRS | CSRRC => {
            let rs = Reg::try_from(imm5, addr)?;
            let csr = CSR::try_from(imm12, addr)?;

            Ok(Instruction {
                instr: EI(CSRI(CSRInstruction { id, rd, rs, csr })),
                compressed: false,
                bytes,
                addr,
            })
        }
        CSRRWI | CSRRSI | CSRRCI => {
            let csr = CSR::try_from(imm12, addr)?;

            Ok(Instruction {
                instr: EI(CSRII(CSRIInstruction { id, rd, imm5, csr })),
                compressed: false,
                bytes,
                addr,
            })
        }
        _ => unreachable!(),
    }
}

pub fn parse_finstruction(addr: Addr, opcode: OpCode, bytes: u32) -> PResult<Instruction> {
    use FInstruction::*;
    if matches!(opcode, OpCode::Nop) {
        let instr = Instruction {
            instr: BaseInstruction::F(NOP),
            compressed: false,
            bytes,
            addr,
        };

        if bytes != 0 {
            Err(PError::InvalidInstruction(instr))
        } else {
            Ok(instr)
        }
    } else {
        let id = FId::try_from(imm!(bytes, [14, -12]), addr)?;
        let imm14: u16 = imm!(bytes, [31, -28, 19, -15, 11, -7]);
        let fence_imm8 = imm!(bytes, [27, -20]);
        use FId::*;
        match id {
            FENCE => {
                let instr = Instruction {
                    instr: BaseInstruction::F(Fence(fence_imm8)),
                    compressed: false,
                    bytes,
                    addr,
                };

                if imm14 != 0 {
                    Err(PError::InvalidInstruction(instr))
                } else {
                    Ok(instr)
                }
            }
            FENCEI => {
                let instr = Instruction {
                    instr: BaseInstruction::F(FenceI),
                    compressed: false,
                    bytes,
                    addr,
                };

                if imm14 != 0 || fence_imm8 != 0 {
                    Err(PError::InvalidInstruction(instr))
                } else {
                    Ok(instr)
                }
            }
        }
    }
}

pub fn parse_sinstruction(addr: Addr, _opcode: OpCode, bytes: u32) -> PResult<Instruction> {
    let id = SId::try_from(imm!(bytes, [14, -12]), addr)?;
    let rs1 = Reg::try_from(imm!(bytes, [19, -15]), addr)?;
    let rs2 = Reg::try_from(imm!(bytes, [24, -20]), addr)?;
    let imm12 = imm!(bytes, [31, -25, 11, -7]);

    Ok(Instruction {
        instr: S(SInstruction {
            id,
            rs1,
            rs2,
            imm12,
        }),
        compressed: false,
        bytes,
        addr,
    })
}

pub fn parse_binstruction(addr: Addr, _opcode: OpCode, bytes: u32) -> PResult<Instruction> {
    let id = BId::try_from(imm!(bytes, [14, -12]), addr)?;
    let rs1 = Reg::try_from(imm!(bytes, [19, -15]), addr)?;
    let rs2 = Reg::try_from(imm!(bytes, [24, -20]), addr)?;
    let imm12 = imm!(bytes, [31, -25, 11, -7], [12, 10, -5, 4, -1, 11]);

    Ok(Instruction {
        instr: B(BInstruction {
            id,
            rs1,
            rs2,
            imm12,
        }),
        compressed: false,
        bytes,
        addr,
    })
}

pub fn parse_uinstruction(addr: Addr, opcode: OpCode, bytes: u32) -> PResult<Instruction> {
    let rd = Reg::try_from(imm!(bytes, [11, -7]), addr)?;
    let imm20 = imm!(bytes, [31, -12]);

    let id = match opcode {
        OpCode::LUI => UId::LUI,
        OpCode::AUIPC => UId::AUIPC,
        _ => unreachable!(),
    };

    Ok(Instruction {
        instr: U(UInstruction { id, rd, imm20 }),
        compressed: false,
        bytes,
        addr,
    })
}

pub fn parse_jinstruction(addr: Addr, _opcode: OpCode, bytes: u32) -> PResult<Instruction> {
    let id = JId::JAL;
    let rd = Reg::try_from(imm!(bytes, [11, -7]), addr)?;
    let imm20 = imm!(bytes, [31, -12], [20, 10, -1, 11, 19, -12]);

    Ok(Instruction {
        instr: J(JInstruction { id, rd, imm20 }),
        compressed: false,
        bytes,
        addr,
    })
}
