struct RangeRevIter<T: Iterator<Item = i8>>(T);

impl<T: Iterator<Item = i8>> Iterator for RangeRevIter<T> {
    type Item = (u8, u8);
    fn next(&mut self) -> Option<Self::Item> {
        match self.0.next() {
            Some(start) if start < 0 => {
                if let Some(end) = self.0.next() {
                    assert!(
                        0 <= end,
                        "Bad range sequence, cannot have twice negative in a row"
                    );
                    Some((end as u8, (start.checked_neg().unwrap_or(0) as u8)))
                } else {
                    panic!("Bad range sequence, cannot begin by a negative number")
                }
            }
            Some(start) => Some((start as u8, start as u8)),
            None => None,
        }
    }
}

pub const NEG_0: i8 = -128;

/// Parse an immediate, given the corresponding bit ranges and the final order of the bits
///
/// Ranges are encoded as an array where the minus defines a range
/// `[a, b, -c, d, e, f, -g]` means
/// `[a..=a, b..=c, d..=d, e..=e, f..=g]`
///
/// # Arguments
/// * __bytes__ the bytes to parse to obtain the immediate value
/// * __imm_ranges__ defines from msb to lsb the ranges of bits corresponding to the immediate to parse
/// * __order_ranges__ defines the order in which the bits must be rearranged to obtain the immediate value
///
/// If all bytes correspond to the immediate then __imm_ranges__ can be empty
/// 
/// If the corresponding bytes are already in order then __order_ranges__ can be empty 
/// 
/// If a zero-ended range is needed, use NEG_0 (-128) 
/// 
/// # Example
///
/// let say we want to parse branches instructions, the risc-v doc says :
///
///     31 - 25 | 24 - 20 | 19 - 15 | 14 - 12 | 11 - 7 | 6 - 0
///       imm       rs2       rs1      func       imm      op
///
/// So the immediate is defined by two bit ranges, 31 - 25 and 11 - 7.
///
/// Next the docs tells us the order of the bits :
///
///     31  -   25  | ... | 11   -   7  | ...
///     12, 10 : 5  | ... |  4 - 1, 11  | ...
///
/// We can then make the correspondance with the bit ranges
///
/// imm = bytes[31] << 12 + bytes[30..=25] << 5 + bytes[11..=8] << 1 + bytes[7] << 11
///
/// Which we write :
/// parse_immediate(bytes, &[31, -25, 11, -7], &[12, 10, -5, 4, -1, 11])
///
pub fn parse_immediate<In: Into<u128>, Out: TryFrom<u128>>(
    bytes: In,
    imm_ranges: &[i8],
    order_ranges: &[i8],
) -> Result<Out, Out::Error> {
    let bytes: u128 = bytes.into();
    let mut imm_bytes: u128 = 0;
    let mut imm_bytes_length = 0;

    if imm_ranges.is_empty() {
        imm_bytes = bytes;
    }

    for (end, start) in RangeRevIter(imm_ranges.into_iter().rev().copied()) {
        assert!(end >= start, "Bad range sequence");
        let length = end - start + 1;
        imm_bytes += ((bytes >> start) & ((1 << length) - 1)) << imm_bytes_length;
        imm_bytes_length += length;
    }

    if order_ranges.is_empty() {
        return imm_bytes.try_into();
    }

    let mut imm = 0;
    let mut imm_length = 0;
    for (end, start) in RangeRevIter(order_ranges.into_iter().rev().copied()) {
        let length = end - start + 1;
        imm += (imm_bytes & ((1 << length) - 1)) << start;
        imm_bytes >>= length;
        imm_length += length;
    }

    if !imm_ranges.is_empty() && imm_bytes_length != imm_length {
        eprintln!("Length should be equal, there are probably somme error in the sequences:");
        eprintln!(
            "imm_bytes with length {}: {:?}",
            imm_bytes_length, imm_ranges,
        );
        eprintln!("imm       with length {}: {:?}", imm_length, order_ranges);
        panic!("Sequence error, lengths should be equal")
    }

    imm.try_into()
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn p1() {
        let a =
            parse_immediate::<u16, u16>(0xF0FF, &[15, -8, 3, 2, 1, 0], &[11, -8, 7, -4, 3, NEG_0])
                .unwrap();
        let b = 0x0F0F;
        assert_eq!(a, b, "testing {:x} and {:x}", a, b);
    }

    #[test]
    fn p1_2() {
        let a =
            parse_immediate::<u16, u16>(0xF0FF, &[15, -8, 3, 2, 1, 0], &[11, -8, 15, -12, 3, NEG_0])
                .unwrap();
        let b = 0x0F0F;
        assert_eq!(a, b, "testing {:x} and {:x}", a, b);
    }

    #[test]
    fn p1_3() {
        let a = parse_immediate::<u32, u32>(0xe882a283, &[31, -20], &[11, NEG_0]).unwrap();
        let b = 0x0e88;
        assert_eq!(a, b, "testing 0x{:04x} and 0x{:04x}", a, b);
    }

    #[test]
    fn p1_4() {
        let a =
            parse_immediate::<u32, u32>(0xe882a283, &[31, -12], &[20, 10, -1, 11, 19, -12]).unwrap();
        let b = 0b1_00101010_0_1101000100_0;
        assert_eq!(a, b, "testing 0x{:04x} and 0x{:04x}", a, b);
    }

    #[test]
    fn p1_5() {
        let a =
            parse_immediate::<u32, u32>(0x35010113, &[31, -20], &[]).unwrap();
        let b = 0x350;
        assert_eq!(a, b, "testing 0x{:04x} and 0x{:04x}", a, b);
    }

    

    #[test]
    #[should_panic]
    fn p2() {
        let _ = parse_immediate::<u16, u16>(
            0xF0FF,
            &[15, -12, 7, -8, 3, 2, 1, 0, 11, -8],
            &[7, -4, 15, -8, 3, NEG_0],
        );
    }

    #[test]
    #[should_panic]
    fn p3() {
        let _ = parse_immediate::<u16, u16>(
            0xF0FF,
            &[-15, -12, 7, -4, 3, 2, 1, 0, 11, -8],
            &[7, -4, 15, -8, 3, NEG_0],
        );
    }

    #[test]
    #[should_panic]
    fn p4() {
        let _ = parse_immediate::<u16, u16>(
            0xF0FF,
            &[15, -12, 7, -4, -3, 2, 1, 0, 11, -8],
            &[7, -4, 15, -8, 3, NEG_0],
        );
    }
}
