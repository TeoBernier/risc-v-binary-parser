use std::fmt::{self, Debug, Display, Formatter};

use crate::inner::*;

#[repr(u8)]
#[allow(non_camel_case_types)]
#[derive(Debug, Clone, Copy, Hash, PartialOrd, Ord, PartialEq, Eq)]
pub enum Reg {
    zero = 0,
    ra = 1,
    sp = 2,
    gp = 3,
    tp = 4,
    t0 = 5,
    t1 = 6,
    t2 = 7,
    fp = 8,
    s1 = 9,
    a0 = 10,
    a1 = 11,
    a2 = 12,
    a3 = 13,
    a4 = 14,
    a5 = 15,
    a6 = 16,
    a7 = 17,
    s2 = 18,
    s3 = 19,
    s4 = 20,
    s5 = 21,
    s6 = 22,
    s7 = 23,
    s8 = 24,
    s9 = 25,
    s10 = 26,
    s11 = 27,
    t3 = 28,
    t4 = 29,
    t5 = 30,
    t6 = 31,
}

impl Reg {
    pub fn try_from(value: u8, addr: Addr) -> PResult<Self> {
        use Reg::*;
        match value {
            0 => Ok(zero),
            1 => Ok(ra),
            2 => Ok(sp),
            3 => Ok(gp),
            4 => Ok(tp),
            5 => Ok(t0),
            6 => Ok(t1),
            7 => Ok(t2),
            8 => Ok(fp),
            9 => Ok(s1),
            10 => Ok(a0),
            11 => Ok(a1),
            12 => Ok(a2),
            13 => Ok(a3),
            14 => Ok(a4),
            15 => Ok(a5),
            16 => Ok(a6),
            17 => Ok(a7),
            18 => Ok(s2),
            19 => Ok(s3),
            20 => Ok(s4),
            21 => Ok(s5),
            22 => Ok(s6),
            23 => Ok(s7),
            24 => Ok(s8),
            25 => Ok(s9),
            26 => Ok(s10),
            27 => Ok(s11),
            28 => Ok(t3),
            29 => Ok(t4),
            30 => Ok(t5),
            31 => Ok(t6),
            _ => Err(UnknownReg(addr, value)),
        }
    }

    pub fn try_from_compressed(value: u8, addr: Addr) -> PResult<Self> {
        use Reg::*;
        match value {
            8 => Ok(fp),
            9 => Ok(s1),
            10 => Ok(a0),
            11 => Ok(a1),
            12 => Ok(a2),
            13 => Ok(a3),
            14 => Ok(a4),
            15 => Ok(a5),
            _ => Err(UnknownReg(addr, value)),
        }
    }
}

impl Display for Reg {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        Debug::fmt(self, f)
    }
}
