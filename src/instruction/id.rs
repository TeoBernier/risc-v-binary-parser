use std::fmt::{self, Display, Formatter};

use crate::inner::*;
use OpCode::*;

#[repr(u8)]
#[derive(Debug, Clone, Copy, Hash, PartialOrd, Ord, PartialEq, Eq)]
pub enum OpCode {
    // No OPeration
    Nop = 0b0000000,

    // Binary Operation
    BinOp = 0b0110011,

    // Atomic Binary Operation
    ABinOp = 0b0101111,

    // Binary Operation with Immediate
    BinOpImm = 0b0010011,

    // Load
    Load = 0b0000011,

    // Fence
    Fence = 0b0001111,

    // Store
    Store = 0b0100011,

    // Branch
    Branch = 0b1100011,

    // Jump And Link
    JAL = 0b1101111,

    // Jump And Link
    JALR = 0b1100111,

    // Environment
    Environment = 0b1110011,

    // Load Upper Immediate
    LUI = 0b0110111,

    // Add Upper Immediate to Pointer Counter
    AUIPC = 0b0010111,

    // OpCode for compressed instruction
    Compressed(u8) = 0xFF,
}

impl OpCode {
    pub fn try_from(value: u8, addr: Addr) -> PResult<Self> {
        match value {
            0b0000000 => Ok(Nop),
            0b0110011 => Ok(BinOp),
            0b0101111 => Ok(ABinOp),
            0b0010011 => Ok(BinOpImm),
            0b0000011 => Ok(Load),
            0b0001111 => Ok(Fence),
            0b0100011 => Ok(Store),
            0b1100011 => Ok(Branch),
            0b1101111 => Ok(JAL),
            0b1100111 => Ok(JALR),
            0b1110011 => Ok(Environment),
            0b0110111 => Ok(LUI),
            0b0010111 => Ok(AUIPC),
            _ => Err(UnknownOpCode(addr, value)),
        }
    }
}

#[repr(u16)]
#[derive(Debug, Clone, Copy, Hash, PartialOrd, Ord, PartialEq, Eq)]
pub enum RId {
    // Add
    ADD = 0x0,

    // Sub
    SUB = 0x100,

    // Shift Logical Left
    SLL = 0x1,

    // Set Less Than (signed)
    SLT = 0x2,

    // Set Less Than Unsigned
    SLTU = 0x3,

    // Xor
    XOR = 0x4,

    // Shift Right Logical
    SRL = 0x5,

    // Shift Right Arithmetic
    SRA = 0x105,

    // Or
    OR = 0x6,

    // And
    AND = 0x7,

    // Mul (signed)
    MUL = 0x8,

    // Mul upper Half (signed)
    MULH = 0x9,

    // Mul upper Half Signed / Unsigned
    MULHSU = 0xA,

    // Mul upper Half Unsigned
    MULHU = 0xB,

    // Div
    DIV = 0xC,

    // Div Unsigned
    DIVU = 0xD,

    // Rem
    REM = 0xE,

    // Rem Unsigned
    REMU = 0xF,
}

impl RId {
    pub fn try_from(value: u16, addr: Addr) -> PResult<Self> {
        use RId::*;
        match value {
            0x0 => Ok(ADD),
            0x100 => Ok(SUB),
            0x1 => Ok(SLL),
            0x2 => Ok(SLT),
            0x3 => Ok(SLTU),
            0x4 => Ok(XOR),
            0x5 => Ok(SRL),
            0x105 => Ok(SRA),
            0x6 => Ok(OR),
            0x7 => Ok(AND),
            0x8 => Ok(MUL),
            0x9 => Ok(MULH),
            0xA => Ok(MULHSU),
            0xB => Ok(MULHU),
            0xC => Ok(DIV),
            0xD => Ok(DIVU),
            0xE => Ok(REM),
            0xF => Ok(REMU),
            _ => Err(UnknownFuncCode(addr, OpCode::BinOp, value as u32)),
        }
    }

    pub fn opcode(&self) -> OpCode {
        OpCode::BinOp
    }
}

impl Display for RId {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        use RId::*;
        let s = match self {
            ADD => "add",
            SUB => "sub",
            SLL => "sll",
            SLT => "slt",
            SLTU => "sltu",
            XOR => "xor",
            SRL => "srl",
            SRA => "sra",
            OR => "or",
            AND => "and",
            MUL => "mul",
            MULH => "mulh",
            MULHSU => "mulhsu",
            MULHU => "mulhu",
            DIV => "div",
            DIVU => "divu",
            REM => "rem",
            REMU => "remu",
        };
        Display::fmt(s, f)
    }
}

#[repr(u8)]
#[derive(Debug, Clone, Copy, Hash, PartialOrd, Ord, PartialEq, Eq)]
pub enum ARId {
    // Atomic Add
    AMOADD = 0x02,

    // Atomic Swap
    AMOSWAP = 0x0A,

    // Load Reserved
    LR = 0x12,

    // Store Conditional
    SC = 0x1A,

    // Atomic Xor
    AMOXOR = 0x22,

    // Atomic Or
    AMOOR = 0x52,

    // Atomic And
    AMOAND = 0x62,

    // Atomic Min
    AMOMIN = 0x82,

    // Atomic Min Unsigned
    AMOMINU = 0xC2,

    // Atomic Max
    AMOMAX = 0xA2,

    // Atomic Max Unsigned
    AMOMAXU = 0xE2,
}

impl ARId {
    pub fn try_from(value: u8, addr: Addr) -> PResult<Self> {
        use ARId::*;
        match value {
            0x02 => Ok(AMOADD),
            0x0A => Ok(AMOSWAP),
            0x12 => Ok(LR),
            0x1A => Ok(SC),
            0x22 => Ok(AMOXOR),
            0x52 => Ok(AMOOR),
            0x62 => Ok(AMOAND),
            0x82 => Ok(AMOMIN),
            0xC2 => Ok(AMOMINU),
            0xA2 => Ok(AMOMAX),
            0xE2 => Ok(AMOMAXU),
            _ => Err(UnknownFuncCode(addr, OpCode::ABinOp, value as u32)),
        }
    }

    pub fn opcode(&self) -> OpCode {
        OpCode::ABinOp
    }
}

impl Display for ARId {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        use ARId::*;
        let s = match self {
            AMOADD => "amoadd",
            AMOSWAP => "amoswap",
            LR => "lr",
            SC => "sc",
            AMOXOR => "amoxor",
            AMOOR => "amoor",
            AMOAND => "amoand",
            AMOMIN => "amomin",
            AMOMINU => "amominu",
            AMOMAX => "amomax",
            AMOMAXU => "amomaxu",
        };
        Display::fmt(s, f)
    }
}

#[repr(u16)]
#[derive(Debug, Clone, Copy, Hash, PartialOrd, Ord, PartialEq, Eq)]
pub enum BinOpIId {
    // Add Immediate
    ADDI = 0x0,

    // Shift Logical Left Immediate
    SLLI = 0x1,

    // Set Less Than Immediate (signed)
    SLTI = 0x2,

    // Set Less Than Immediate Unsigned
    SLTIU = 0x3,

    // Xor Immediate
    XORI = 0x4,

    // Shift Right Logical Immediate
    SRLI = 0x5,

    // Shift Right Arithmetic Immediate
    SRAI = 0x105,

    // Or Immediate
    ORI = 0x6,

    // And Immediate
    ANDI = 0x7,
}

impl BinOpIId {
    pub fn try_from(value: u16, addr: Addr) -> PResult<Self> {
        use BinOpIId::*;
        match value {
            0x0 => Ok(ADDI),
            0x1 => Ok(SLLI),
            0x2 => Ok(SLTI),
            0x3 => Ok(SLTIU),
            0x4 => Ok(XORI),
            0x5 => Ok(SRLI),
            0x105 => Ok(SRAI),
            0x6 => Ok(ORI),
            0x7 => Ok(ANDI),
            _ => Err(UnknownFuncCode(addr, OpCode::BinOpImm, value as u32)),
        }
    }
}

impl Display for BinOpIId {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        use BinOpIId::*;
        let s = match self {
            ADDI => "addi",
            SLLI => "slli",
            SLTI => "slti",
            SLTIU => "sltiu",
            XORI => "xori",
            SRLI => "srli",
            SRAI => "srai",
            ORI => "ori",
            ANDI => "andi",
        };
        Display::fmt(s, f)
    }
}

#[repr(u8)]
#[derive(Debug, Clone, Copy, Hash, PartialOrd, Ord, PartialEq, Eq)]
pub enum LoadIId {
    // Load Byte (signed)
    LB = 0x0,

    // Load Half
    LH = 0x1,

    // Load Word
    LW = 0x2,

    // Load Byte Unsigned
    LBU = 0x4,

    // Load Half Unsigned
    LHU = 0x5,
}

impl LoadIId {
    pub fn try_from(value: u8, addr: Addr) -> PResult<Self> {
        use LoadIId::*;
        match value {
            0x0 => Ok(LB),
            0x1 => Ok(LH),
            0x2 => Ok(LW),
            0x4 => Ok(LBU),
            0x5 => Ok(LHU),
            _ => Err(UnknownFuncCode(addr, OpCode::Load, value as u32)),
        }
    }
}

impl Display for LoadIId {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        use LoadIId::*;
        let s = match self {
            LB => "lb",
            LH => "lh",
            LW => "lw",
            LBU => "lbu",
            LHU => "lhu",
        };
        Display::fmt(s, f)
    }
}

#[derive(Debug, Clone, Copy, Hash, PartialOrd, Ord, PartialEq, Eq)]
pub enum IId {
    BinOp(BinOpIId),
    Load(LoadIId),
    JALR,
}

impl Display for IId {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        use IId::*;
        match self {
            BinOp(id) => Display::fmt(id, f),
            Load(id) => Display::fmt(id, f),
            JALR => Display::fmt("jalr", f),
        }
    }
}

impl IId {
    pub fn opcode(&self) -> OpCode {
        use OpCode::*;
        match self {
            IId::BinOp(..) => BinOpImm,
            IId::Load(..) => Load,
            IId::JALR => JALR,
        }
    }
}

#[repr(u16)]
#[derive(Debug, Clone, Copy, Hash, PartialOrd, Ord, PartialEq, Eq)]
pub enum EIId {
    // Environment Call
    ECALL = 0x0,

    // Environment Break
    EBREAK = 0x8,

    // Supervisor-mode exception Return
    SRET = 0x0810,

    // Machine-mode exception Return
    MRET = 0x1810,

    // Wait For Interrupt
    WFI = 0x0828,

    // Fence Virtual Memory
    SFENCEVMA = 0x0900,

    // Control Status Register Read Write
    CSRRW = 0x1,

    // Control Status Register Read Set bit
    CSRRS = 0x2,

    // Control Status Register Read Clear bit
    CSRRC = 0x3,

    // Control Status Register Read Write Immediate
    CSRRWI = 0x5,

    // Control Status Register Read Set bit Immediate
    CSRRSI = 0x6,

    // Control Status Register Read Clear bit Immediate
    CSRRCI = 0x7,
}

impl EIId {
    pub fn try_from(value: u16, addr: Addr) -> PResult<Self> {
        use EIId::*;
        match value {
            0x0 => Ok(ECALL),
            0x8 => Ok(EBREAK),
            0x0810 => Ok(SRET),
            0x1810 => Ok(MRET),
            0x0828 => Ok(WFI),
            0x0900 => Ok(SFENCEVMA),
            0x1 => Ok(CSRRW),
            0x2 => Ok(CSRRS),
            0x3 => Ok(CSRRC),
            0x5 => Ok(CSRRWI),
            0x6 => Ok(CSRRSI),
            0x7 => Ok(CSRRCI),
            _ => Err(UnknownFuncCode(addr, OpCode::Environment, value as u32)),
        }
    }
}

impl Display for EIId {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        use EIId::*;
        let s = match self {
            ECALL => "ecall",
            EBREAK => "ebreak",
            SRET => "sret",
            MRET => "mret",
            WFI => "wfi",
            SFENCEVMA => "sfence.vma",
            CSRRW => "csrrw",
            CSRRS => "csrrs",
            CSRRC => "csrrc",
            CSRRWI => "csrrwi",
            CSRRSI => "csrrsi",
            CSRRCI => "csrrci",
        };
        Display::fmt(s, f)
    }
}

pub enum FId {
    FENCE = 0x0,
    FENCEI = 0x1,
}

impl FId {
    pub fn try_from(value: u8, addr: Addr) -> PResult<Self> {
        use FId::*;
        match value {
            0x0 => Ok(FENCE),
            0x1 => Ok(FENCEI),
            _ => Err(UnknownFuncCode(addr, OpCode::Fence, value as u32)),
        }
    }
}

impl Display for FId {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        use FId::*;
        let s = match self {
            FENCE => "fence",
            FENCEI => "fence.i",
        };
        Display::fmt(s, f)
    }
}

#[repr(u8)]
#[derive(Debug, Clone, Copy, Hash, PartialOrd, Ord, PartialEq, Eq)]
pub enum SId {
    // Store Byte
    SB = 0x0,

    // Store Half
    SH = 0x1,

    // Store Word
    SW = 0x2,
}

impl SId {
    pub fn try_from(value: u8, addr: Addr) -> PResult<Self> {
        use SId::*;
        match value {
            // Store Byte
            0x0 => Ok(SB),

            // Store Half
            0x1 => Ok(SH),

            // Store Word
            0x2 => Ok(SW),

            _ => Err(UnknownFuncCode(addr, OpCode::Store, value as u32)),
        }
    }

    pub fn opcode(&self) -> OpCode {
        OpCode::Store
    }
}

impl Display for SId {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        use SId::*;
        let s = match self {
            SB => "sb",
            SH => "sh",
            SW => "sw",
        };
        Display::fmt(s, f)
    }
}

#[repr(u8)]
#[derive(Debug, Clone, Copy, Hash, PartialOrd, Ord, PartialEq, Eq)]
pub enum BId {
    // Branch if Equal
    BEQ = 0x0,

    // Branch if Non Equal
    BNE = 0x1,

    // Branch if Lower Than
    BLT = 0x4,

    // Branch if Greater Equal
    BGE = 0x5,

    // Branch if Lower Than Unsigned
    BLTU = 0x6,

    // Branch if Greater Equal Unsigned
    BGEU = 0x7,
}

impl BId {
    pub fn try_from(value: u8, addr: Addr) -> PResult<Self> {
        use BId::*;
        match value {
            // Branch if Equal
            0x0 => Ok(BEQ),

            // Branch if Non Equal
            0x1 => Ok(BNE),

            // Branch if Lower Than
            0x4 => Ok(BLT),

            // Branch if Greater Equal
            0x5 => Ok(BGE),

            // Branch if Lower Than Unsigned
            0x6 => Ok(BLTU),

            // Branch if Greater Equal Unsigned
            0x7 => Ok(BGEU),

            _ => Err(UnknownFuncCode(addr, OpCode::Store, value as u32)),
        }
    }

    pub fn opcode(&self) -> OpCode {
        OpCode::Branch
    }
}

impl Display for BId {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        use BId::*;
        let s = match self {
            BEQ => "beq",
            BNE => "bne",
            BLT => "blt",
            BGE => "bge",
            BLTU => "bltu",
            BGEU => "bgeu",
        };
        Display::fmt(s, f)
    }
}

#[derive(Debug, Clone, Copy, Hash, PartialOrd, Ord, PartialEq, Eq)]
pub enum UId {
    // Load Upper Immediate
    LUI,

    // Add Upper Immediate to Pointer Counter
    AUIPC,
}

impl UId {
    pub fn opcode(&self) -> OpCode {
        match self {
            UId::LUI => OpCode::LUI,
            UId::AUIPC => OpCode::AUIPC,
        }
    }
}

impl Display for UId {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        use UId::*;
        let s = match self {
            LUI => "lui",
            AUIPC => "auipc",
        };
        Display::fmt(s, f)
    }
}

#[derive(Debug, Clone, Copy, Hash, PartialOrd, Ord, PartialEq, Eq)]
pub enum JId {
    // Jump And Link
    JAL,
}

impl JId {
    pub fn opcode(&self) -> OpCode {
        OpCode::JAL
    }
}

impl Display for JId {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        use JId::*;
        let s = match self {
            JAL => "jal",
        };
        Display::fmt(s, f)
    }
}
