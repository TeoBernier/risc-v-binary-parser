use std::fmt::{self, Display, Formatter};

use colored::Colorize;

use crate::inner::*;

fn format_id<T: Display>(id: T) -> impl Display {
    format!("{:<6}", id).red()
}

fn format_reg(reg: Reg) -> impl Display {
    format!("{:<3}", reg.to_string()).blue()
}

fn format_csr(csr: CSR) -> impl Display {
    format!("{}", csr.to_string()).magenta()
}

fn format_imm(imm: u32) -> impl Display {
    format!("0x{:04x}", imm).green()
}

fn format_arg(arg: &str) -> impl Display {
    format!("{}", arg).magenta()
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct RInstruction {
    pub id: RId,
    pub rd: Reg,
    pub rs1: Reg,
    pub rs2: Reg,
}

impl Display for RInstruction {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "{} {} {} {}",
            format_id(self.id),
            format_reg(self.rd),
            format_reg(self.rs1),
            format_reg(self.rs2)
        )
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct ARInstruction {
    pub id: ARId,
    pub rd: Reg,
    pub rs1: Reg,
    pub rs2: Reg,
    pub rl: bool,
    pub aq: bool,
}

impl Display for ARInstruction {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        let aq = if self.aq { ".aq" } else { "" };
        let rl = if self.rl { ".rl" } else { "" };
        let id = format!("{}{}{}", format_id(self.id), aq, rl);
        write!(
            f,
            "{} {} {} {}",
            format_id(id),
            format_reg(self.rd),
            format_reg(self.rs1),
            format_reg(self.rs2)
        )
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct IInstruction {
    pub id: IId,
    pub rd: Reg,
    pub rs: Reg,
    pub imm12: u16,
}

impl Display for IInstruction {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "{} {} {} {}",
            format_id(self.id),
            format_reg(self.rd),
            format_reg(self.rs),
            format_imm(self.imm12 as u32)
        )
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct SpecialInstruction {
    pub id: EIId,
}

impl Display for SpecialInstruction {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        Display::fmt(&format_id(self.id), f)
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct CSRInstruction {
    pub id: EIId,
    pub rd: Reg,
    pub rs: Reg,
    pub csr: CSR,
}

impl Display for CSRInstruction {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "{} {} {} {}",
            format_id(self.id),
            format_reg(self.rd),
            format_csr(self.csr),
            format_reg(self.rs),
        )
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct CSRIInstruction {
    pub id: EIId,
    pub rd: Reg,
    pub imm5: u8,
    pub csr: CSR,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub enum EIInstruction {
    SFenceInstruction(Reg, Reg),
    SI(SpecialInstruction),
    CSRI(CSRInstruction),
    CSRII(CSRIInstruction),
}

impl Display for CSRIInstruction {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "{} {} {} {}",
            format_id(self.id),
            format_reg(self.rd),
            format_csr(self.csr),
            format_imm(self.imm5 as u32),
        )
    }
}

impl Display for EIInstruction {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        use EIInstruction::*;
        match self {
            &SFenceInstruction(rs1, rs2) => write!(
                f,
                "{} {} {}",
                format_id(&EIId::SFENCEVMA),
                format_reg(rs1),
                format_reg(rs2)
            ),
            SI(instr) => Display::fmt(instr, f),
            CSRI(instr) => Display::fmt(instr, f),
            CSRII(instr) => Display::fmt(instr, f),
        }
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub enum FInstruction {
    Fence(u8),
    FenceI,
    NOP,
    Illegal,
    Unimplemented,
}

fn get_fence_arg(imm4: u8) -> String {
    let mut arg = String::new();
    if imm4 & 0x8 != 0 {
        arg.push('i');
    }
    if imm4 & 0x4 != 0 {
        arg.push('o');
    }
    if imm4 & 0x2 != 0 {
        arg.push('r');
    }
    if imm4 & 0x1 != 0 {
        arg.push('w');
    }
    arg
}

impl Display for FInstruction {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        use FInstruction::*;
        match self {
            &Fence(imm8) => {
                let pred = get_fence_arg(imm8 >> 4);
                let succ = get_fence_arg(imm8 & 0xF);
                write!(
                    f,
                    "{} {} {}",
                    format_id(FId::FENCE),
                    format_arg(&pred),
                    format_arg(&succ)
                )
            }

            NOP => Display::fmt(&format_id("nop"), f),
            FenceI => Display::fmt(&format_id(FId::FENCEI), f),
            Unimplemented => Display::fmt(&format_id("unimp"), f),
            Illegal => Display::fmt(&format_id("illegal"), f),
        }
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct SInstruction {
    pub id: SId,
    pub rs1: Reg,
    pub rs2: Reg,
    pub imm12: u16,
}

impl Display for SInstruction {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "{} {} {} {}",
            format_id(self.id),
            format_reg(self.rs1),
            format_reg(self.rs2),
            format_imm(self.imm12 as u32)
        )
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct BInstruction {
    pub id: BId,
    pub rs1: Reg,
    pub rs2: Reg,
    pub imm12: u16,
}

impl Display for BInstruction {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "{} {} {} {}",
            format_id(self.id),
            format_reg(self.rs1),
            format_reg(self.rs2),
            format_imm(self.imm12 as u32)
        )
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct UInstruction {
    pub id: UId,
    pub rd: Reg,
    pub imm20: u32,
}

impl Display for UInstruction {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "{} {} {}",
            format_id(self.id),
            format_reg(self.rd),
            format_imm(self.imm20)
        )
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct JInstruction {
    pub id: JId,
    pub rd: Reg,
    pub imm20: u32,
}

impl Display for JInstruction {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "{} {} {}",
            format_id(self.id),
            format_reg(self.rd),
            format_imm(self.imm20)
        )
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub enum BaseInstruction {
    R(RInstruction),
    AR(ARInstruction),
    I(IInstruction),
    EI(EIInstruction),
    F(FInstruction),
    S(SInstruction),
    B(BInstruction),
    U(UInstruction),
    J(JInstruction),
}

impl Display for BaseInstruction {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        use BaseInstruction::*;
        match self {
            R(instr) => Display::fmt(instr, f),
            AR(instr) => Display::fmt(instr, f),
            I(instr) => Display::fmt(instr, f),
            EI(instr) => Display::fmt(instr, f),
            F(instr) => Display::fmt(instr, f),
            S(instr) => Display::fmt(instr, f),
            B(instr) => Display::fmt(instr, f),
            U(instr) => Display::fmt(instr, f),
            J(instr) => Display::fmt(instr, f),
        }
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct Addr(pub u32);

impl Display for Addr {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        Display::fmt(&self.0, f)
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct Instruction {
    pub instr: BaseInstruction,
    pub compressed: bool,
    pub bytes: u32,
    pub addr: Addr,
}

impl Display for Instruction {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        let addr = format!("{:08X}", self.addr.0);
        let bytes = if self.compressed {format!("{:04x}", self.bytes)} else {format!("{:08x}", self.bytes)};
        write!(f, "{}:  {:>8}  {}", addr.cyan(), bytes.yellow(), self.instr)
    }
}
