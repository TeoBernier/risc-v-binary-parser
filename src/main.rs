use std::collections::HashMap;

use binary_parsing::parsing::parse_bytes_no_error;
use binary_parsing::{instruction::Addr, parsing::parse_bytes};

use colored::Colorize;
use elf::*;

pub fn get_code<'a>(elf: &'a ElfBytes<endian::AnyEndian>) -> (usize, usize, u64) {
    if let Some(segs) = elf.segments() {
        for phdr in segs {
            if phdr.p_flags == elf::abi::PF_R | elf::abi::PF_X {
                return (phdr.p_offset as usize, phdr.p_filesz as usize, phdr.p_vaddr);
            }
        }
    }
    panic!()
}

pub fn main() {
    let args = std::env::args().collect::<Vec<_>>();

    let path = &args[1];

    let data = match std::fs::read(path) {
        Ok(raw) => raw,
        Err(e) => {
            println!("{} Failed to read '{}'. {}", "ERROR:".red(), path, e);
            return;
        }
    };

    let Ok(elf) =ElfBytes::<endian::AnyEndian>::minimal_parse(&data) else {
        panic!( "{} Failed to parse '{}'. Make sure to provide a valid ELF file",
        "ERROR:".red(),
        path
    )};

    if elf.ehdr.e_machine != elf::abi::EM_RISCV || elf.ehdr.class != elf::file::Class::ELF32 {
        println!(
            "{} racoonv only supports Risc-V binaries (ISA RV32IC)",
            "ERROR:".red()
        );
        return;
    }

    let Ok(Some((symbols, str_table))) = elf.symbol_table() else {
        panic!( "{} Failed to parse symbols and associated string table in '{}'. Make sure to provide a valid ELF file",
        "ERROR:".red(),
        path
    )};
    let mut symbols_map = HashMap::new();
    for symbol in symbols {
        symbols_map.insert(
            symbol.st_value,
            str_table.get(symbol.st_name as usize).unwrap(),
        );
    }

    let Ok((Some(section_headers), Some(str_table))) = elf.section_headers_with_strtab() else {
        panic!( "{} Failed to parse section headers and associated string table in '{}'. Make sure to provide a valid ELF file",
        "ERROR:".red(),
        path
    )};

    let Some(segments) = elf.segments() else {
        panic!( "{} Failed to parse section headers and associated string table in '{}'. Make sure to provide a valid ELF file",
        "ERROR:".red(),
        path
    )};

    let mut first = true;

    for section in section_headers {
        // for segment in segments {
        //     if segment.p_flags & abi::PF_R != 0
        //         && segment.p_flags & abi::PF_X != 0
        //         && segment.p_flags != 0
        //     {
        //         let mut addr = segment.p_paddr;
        //         let offset = segment.p_offset as usize;
        //         let size = segment.p_filesz as usize;
        //         let code = &data[offset..offset + size];

        //         let mut first_sym = true;

        //         for instruction in parse_bytes(code, Addr(addr as u32)).take_while(Result::is_ok) {
        //             if let Some(symbol) = symbols_map.get(&addr) {
        //                 if !first_sym {
        //                     println!("");
        //                 }
        //                 println!("{}:  <{symbol}>", format!("{addr:X}").cyan());
        //             }
        //             first_sym = false;
        //             println!("{}", instruction.unwrap());
        //             addr += 4;
        //         }
        //     }
        if section.sh_flags & (abi::SHF_EXECINSTR as u64) != 0 {
            let name = str_table.get(section.sh_name as usize).unwrap();
            let mut addr = section.sh_addr;
            let offset = section.sh_offset as usize;

            if !first {
                println!("\n");
            }
            first = false;

            println!(
                "{}: {}\n",
                format!("{addr:X}").cyan(),
                format!("<SECTION {name}>").red()
            );
            let size = section.sh_size as usize;
            let code = &data[offset..offset + size];

            let mut first_sym = true;

            for instruction in parse_bytes_no_error(code, Addr(addr as u32)) {
                addr = instruction.addr.0 as u64;
                if let Some(symbol) = symbols_map.get(&addr) {
                    if !first_sym {
                        println!("");
                    }
                    println!("{}:  <{symbol}>", format!("{addr:X}").cyan());
                }
                first_sym = false;
                println!("{}", instruction);
            }
        }
    }
}
